(when (packagep (find-package 'require2))
  (delete-package :require2))
(make-package :require2 :use '(:common-lisp))
(in-package :require2)

(defparameter *packages* '()
  "A-List that holds references to all files loaded by require2.
The CAR of each element is the truename of the file.
The CDR of each element is a reference to the loaded package.")

(in-package :common-lisp-user)

(defun require2 (file-path)
  "Loads into the environment the definitions in the file pointed to by FILE-PATH."
  (let* ((truename   (truename file-path))
         (pkg-name   (gensym))
         (pkg-tmp *package*)
         (pkg-var nil)
         (pkg-data   (assoc truename require2::*packages*
                            :test #'equal)))
    (if pkg-data
        (cdr pkg-data)
        (progn 
          (unwind-protect
               (progn (setf pkg-var   (make-package pkg-name))
                      (setf *package* pkg-var)
                      (load truename)
                      (setf require2::*packages*
                            (acons truename pkg-var require2::*packages*)))
            (setf *package* pkg-tmp))
          pkg-name))))

(defmacro ! (package symbol &rest args)
  "Retrieves from PACKAGE the given SYMBOL.
If SYMBOL represents a function, call said function with the given ARGS."
  (let ((sym  (find-symbol (symbol-name symbol) (eval package))))
    (if (fboundp sym)
        `(funcall #',sym ,@args)
        sym)))
